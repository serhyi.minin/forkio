# **FORKIO**

_We are the best!_

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Installing](#installing)
- [Usage](#usage)
- [Contributing](#contributing)
- [Authors](#authors)
- [License](#license)

## About <a name = "about"></a>

Serhii Minin and Anton Marynych were working on this project. Serhii created _Here what you get_, _Fork Subscription_ and _Revolutionary editor_ sections. He also set up our project using task manager _gulp_. Anton created _Header_ and _People talking_ sections. The main goal of the project was to show knowledge of what we have learned. We used _html_, _css_, _javascript_, _gulp_. We worked in teams for the first time which helped us to learn how to work with _git_ on advanced level. We also improved our soft skills, including patience. We worked in different branches and majority of changes in the project were reviewed by other partner of our team before uploading it to the developer branch. After finishing developing we uploaded it to the main branch and published on Gitlab Pages https://serhyiminin.gitlab.io/forkio/

Forkio-step-project is the second Step Project in DAN.IT Education.

Technologies are used in the project:

- HTML;
- CSS (SCSS);
- JS;
- Node.js;
- npm;
  - gulp;
  - gulp-sass;
  - browser-sync;
  - gulp-minify;
  - terser;
  - gulp-clean-css;
  - gulp-clean;
  - gulp-concat;
  - gulp-imagemin;
  - gulp-autoprefixer.

## Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Installing <a name = "installing"></a>

git clone https://gitlab.com/serhyi.minin/forkio.git

## Usage <a name = "usage"></a>

Any suggestions are welcomed!

## Contributing <a name = "contributing"></a>

- <a href="https://dan-it.com.ua/uk/">DAN.IT Education</a>

## License <a name = "license"></a>

[ICS](http://www.isc.org/)

## Authors <a name = "Authors"></a>

Group FE30

- Anton Marynych
- Serhii Minin
